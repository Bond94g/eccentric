<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Eccentric"); ?>

    <div class="banner-info container">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/include/description_index.php"
            )
        ); ?>
        <a href="portfolio.html" class="btn btn-light btn-style mr-3">My Work</a>
        <a href="#about.html" class="btn btn-outline-light btn-style">Hire Me</a>
    </div>
    </div>

    <div id='stars'></div>
    <div id='stars2'></div>
    <div id='stars3'></div>
    </section>
    <!--  //Main banner section -->
    <!-- w3l-content-photo-5 -->
    <div class="w3l-content-photo-5 py-5" id="about">
        <div class="content-main pt-lg-5 pt-md-3 pb-0">
            <div class="container">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/include/content_index.php"
                        )
                    ); ?>


                </div>
            </div>
        </div>
    </div>
<?php
CModule::IncludeModule('iblock');
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "DETAIL_PAGE_URL", "DETAIL_PICTURE", "PROPERTY_ICON", "PROPERTY_SERVICE");
$arFilter = Array("IBLOCK_ID" => 1, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");

$res = CIBlockElement::GetList(
    Array(),
    $arFilter,
    false,
    Array(),
    $arSelect
);
?>

    <section class="w3l-services-block-3">
        <div class="features-main pb-5 pt-0 text-center">
            <div class="container pb-lg-3">
                <div class="row features">

                    <?php while ($ob = $res->Fetch()) { ?>
                        <div class="col-lg-4 col-md-6 feature-grid">
                            <a href="#url">
                                <div class="feature-body <?= $ob["PROPERTY_SERVICE_VALUE"] ?>">
                                    <div class="feature-img">
                                        <span class="<?= $ob["PROPERTY_ICON_VALUE"] ?>" aria-hidden="true"></span>
                                    </div>
                                    <div class="feature-info mt-4">
                                        <h3 class="feature-titel mb-2"><?= $ob['NAME'] ?></h3>
                                        <p class="feature-text"><?= $ob['PREVIEW_TEXT'] ?>
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
    <!--    -->


    <!--                    <div class="col-lg-4 col-md-6 feature-grid">-->
    <!--                        <a href="#url">-->
    <!--                            <div class="feature-body service2">-->
    <!--                                <div class="feature-img">-->
    <!--                                    <span class="fa fa-laptop icon-fea" aria-hidden="true"></span>-->
    <!--                                </div>-->
    <!--                                <div class="feature-info mt-4">-->
    <!--                                    <h3 class="feature-titel mb-2">Mobile Apps</h3>-->
    <!--                                    <p class="feature-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis nam, minima iste molestiae.-->
    <!--                                    </p>-->
    <!---->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                    <div class="col-lg-4 col-md-6 feature-grid">-->
    <!--                        <a href="#url">-->
    <!--                            <div class="feature-body service3">-->
    <!--                                <div class="feature-img">-->
    <!--                                    <span class="fa fa-codepen" aria-hidden="true"></span>-->
    <!--                                </div>-->
    <!--                                <div class="feature-info mt-4">-->
    <!--                                    <h3 class="feature-titel mb-2">Animation Design</h3>-->
    <!--                                    <p class="feature-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis nam, minima iste molestiae.-->
    <!--                                    </p>-->
    <!--                                    <div class="hover">-->
    <!--                                    </div>-->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                    <div class="col-lg-4 col-md-6 feature-grid">-->
    <!--                        <a href="#url">-->
    <!--                            <div class="feature-body service4">-->
    <!--                                <div class="feature-img">-->
    <!--                                    <span class="fa fa-modx" aria-hidden="true"></span>-->
    <!--                                </div>-->
    <!--                                <div class="feature-info mt-4">-->
    <!--                                    <h3 class="feature-titel mb-2">Photoshop Media</h3>-->
    <!--                                    <p class="feature-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis nam, minima iste molestiae.-->
    <!--                                    </p>-->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                    <div class="col-lg-4 col-md-6 feature-grid">-->
    <!--                        <a href="#url">-->
    <!--                            <div class="feature-body service5">-->
    <!--                                <div class="feature-img">-->
    <!--                                    <span class="fa fa-signal icon-fea" aria-hidden="true"></span>-->
    <!--                                </div>-->
    <!--                                <div class="feature-info mt-4">-->
    <!--                                    <h3 class="feature-titel mb-2">Marketing Business</h3>-->
    <!--                                    <p class="feature-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis nam, minima iste molestiae.-->
    <!--                                    </p>-->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                    <div class="col-lg-4 col-md-6 feature-grid">-->
    <!--                        <a href="#url">-->
    <!--                            <div class="feature-body service6">-->
    <!--                                <div class="feature-img">-->
    <!--                                    <span class="fa fa-paint-brush icon-fea" aria-hidden="true"></span>-->
    <!--                                </div>-->
    <!--                                <div class="feature-info mt-4">-->
    <!--                                    <h3 class="feature-titel mb-2">Development</h3>-->
    <!--                                    <p class="feature-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis nam, minima iste molestiae.-->
    <!--                                    </p>-->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->

    <!-- gallery-7 -->
<? $APPLICATION->IncludeComponent(
    "bitrix:news",
    "template_galery",
    Array(
        "ADD_ELEMENT_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BROWSER_TITLE" => "-",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
        "DETAIL_DISPLAY_TOP_PAGER" => "N",
        "DETAIL_FIELD_CODE" => array(0 => "", 1 => "PREVIEW_PICTURE", 2 => "",),
        "DETAIL_PAGER_SHOW_ALL" => "N",
        "DETAIL_PAGER_TEMPLATE" => "",
        "DETAIL_PAGER_TITLE" => "Страница",
        "DETAIL_PROPERTY_CODE" => array(0 => "", 1 => "",),
        "DETAIL_SET_CANONICAL_URL" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "3",
        "IBLOCK_TYPE" => "content",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
        "LIST_FIELD_CODE" => array(0 => "", 1 => "",),
        "LIST_PROPERTY_CODE" => array(0 => "", 1 => "",),
        "MESSAGE_404" => "",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "NEWS_COUNT" => "20",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PREVIEW_TRUNCATE_LEN" => "",
        "SEF_MODE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N",
        "USE_CATEGORIES" => "N",
        "USE_FILTER" => "N",
        "USE_PERMISSIONS" => "N",
        "USE_RATING" => "N",
        "USE_REVIEW" => "N",
        "USE_RSS" => "N",
        "USE_SEARCH" => "N",
        "USE_SHARE" => "N",
        "VARIABLE_ALIASES" => array("ELEMENT_ID" => "ELEMENT_ID", "SECTION_ID" => "SECTION_ID",)
    )
); ?>
    <!-- //gallery-7 -->
    <!-- iphone home block -->
    <section class="w3l-blog py-5 text-center">
        <div class="container py-lg-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="img-block">
                        <a href="#img">
                            <img src="<?= DEFAULT_TEMPLATE_PATH; ?>/images/frame-mac.png" class="img-fluid"
                                 alt="image"/>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3>Passionate designer & developer who loves simplicity.</h3>
                    <h5 class="mt-3">Fusce fringilla tincidunt laoreet volutpat cras varius sit suscipit.</h5>
                    <p class="mt-4 mb-0"> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laboriosam totam
                        id
                        dolorem, nobis aperiam vero
                        reprehenderit quae explicabo fugiat modi eius quos voluptatum ratione dolorum placeat
                        aliquid
                        rerum, at sapiente provident
                        consequuntur. Vero sed hic omnis, odit in ipsam facilis beatae, assumenda dolore
                        necessitatibus
                        sequi?</p>
                    <a href="blog1.html" class="btn-style btn-primary btn mt-lg-5 mt-4">View my project</a>
                </div>
                <!-- <div class="col-lg-3 col-6 pl-md-3 pl-2 mt-lg-0 mt-md-5 mt-4">
                    <div class="img-block">
                        <a href="#img">
                            <img src="assets/images/p2.jpg" class="img-fluid" alt="image" />
                        </a>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!-- //iphone home block -->
    <!-- testimonial -->
    <section class="w3l-quote-main" id="client">
        <div class="quote py-5">
            <div class="container py-lg-5">
                <div class="row">
                    <div class="col-lg-9">
                        <h4>" There are many variations of passages of Lorem Ipsum available, randomised words which
                            don't look even slightly believable.
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum sed dignissimos
                            explicabo
                            facilis incidunt maiores eos "</h4>
                    </div>
                    <div class="col-lg-3 mt-lg-0 mt-3 text-lg-center tablet-grid">
                        <img src="<?= DEFAULT_TEMPLATE_PATH; ?>/images/client1.jpg" alt="" class="img-fluid"/>
                        <div>
                            <h6 class="mb-0 mt-lg-3">christopher robin</h6>
                            <h5 class="mt-1">- UI/UX Designer </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //testimonial -->

    <!-- home page blog-->


<? $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/blog_index.php"
    )
); ?>


    <!-- //home page blog-->
    <!-- freelance hire me -->
    <section class="w3l-grid-quote py-5">
        <div class="container py-lg-3">
            <h6>I'am available for freelance projects.</h6>
            <h3>Let's work together indeed!</h3>
            <a href="contact.html" class="btn-style btn-primary btn">Get quotes</a>
            <a href="contact" class="btn btn-style btn-light ml-2">Hire me</a>

            <!-- //Footer -->

            <!-- move top -->
            <button onclick="topFunction()" id="movetop" class="editContent" title="Go to top">
                <span class="fa fa-angle-up"></span>
            </button>
            <script>
                // When the user scrolls down 20px from the top of the document, show the button
                window.onscroll = function () {
                    scrollFunction()
                };

                function scrollFunction() {
                    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                        document.getElementById("movetop").style.display = "block";
                    } else {
                        document.getElementById("movetop").style.display = "none";
                    }
                }

                // When the user clicks on the button, scroll to the top of the document
                function topFunction() {
                    document.body.scrollTop = 0;
                    document.documentElement.scrollTop = 0;
                }
            </script>

            <!-- disable body scroll which navbar is in active -->
            <script>
                $(function () {
                    $('.navbar-toggler').click(function () {
                        $('body').toggleClass('noscroll');
                    })
                });
            </script>
            <!-- disable body scroll which navbar is in active -->


            <!-- for blog carousel slider -->
            <script>
                $(document).ready(function () {
                    var owl = $('.owl-carousel');
                    owl.owlCarousel({
                        stagePadding: 20,
                        margin: 15,
                        nav: false,
                        loop: false,
                        responsive: {
                            0: {
                                items: 1
                            },
                            600: {
                                items: 2
                            },
                            1000: {
                                items: 3
                            }
                        }
                    })
                })
            </script>
            <!-- //for blog carousel slider -->


            <!-- stats number counter-->
            <script>
                $('.counter').countUp();
            </script>
            <!-- //stats number counter -->


            </body>

            </html>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>