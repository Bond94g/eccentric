<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="w3l-services-block py-5" id="classes">
    <div class="container py-lg-5 py-md-3">
        <div class="services-block_grids_bottom">
            <div class="row">

                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <div class="col-lg-4 col-md-6 service_grid_btm_left">
                        <div class="service_grid_btm_left1">
                            <div class="service_grid_btm_left2">


                                <span class="<?=$arItem['PROPERTIES']['ADD_ICON']['VALUE']?> editContent" aria-hidden="true"></span>
                                <h5><?=$arItem["NAME"]?></h5>
                                <p><?=$arItem["PREVIEW_TEXT"];?></p>
                            </div>
                            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=" " class="img-fluid" />
                        </div>
                    </div>
                <?endforeach;?>
            </div>
        </div>
    </div>
</div>
