<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="w3l-open-block-services py-5">
    <div class="container py-lg-3">
        <h3 class="head-title">What I Expert in</h3>
        <div class="row mt-5 pt-3">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <div class="col-lg-4 col-md-6">
                    <div class="card text-center">
                        <div class="icon-holder">
                            <span class="<?= $arItem['PROPERTIES']['ICON']['VALUE'] ?> service-icon"
                                  aria-hidden="true"></span>
                        </div>
                        <h4 class="mission"><?= $arItem['NAME'] ?></h4>
                        <div class="open-description">
                            <p><?= $arItem['PREVIEW_TEXT'] ?></p>
                            <a href="#read" class="editContent">Read More</a>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>

        </div>
    </div>
</div>