<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!doctype html>
<html lang="en">

<head>
    <?

    use Bitrix\Main\Page\Asset;

    $APPLICATION->ShowHead();
    ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <?
    Asset::getInstance()->addCss(DEFAULT_TEMPLATE_PATH . "/css/style-starter.css");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/jquery-3.3.1.min.js");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/owl.carousel.js");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/jquery.waypoints.min.js");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/jquery.countup.js");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/jquery-1.7.2.js");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/jquery.quicksand.js");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/jquery.prettyPhoto.js");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/bootstrap.min.js");

    Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">');
    Asset::getInstance()->addString('<link href="//fonts.googleapis.com/css?family=Nunito:400,700&display=swap" rel="stylesheet">');
    ?>
</head>

<body>
<? $APPLICATION->ShowPanel(); ?>
<header>
    <div class="w3l-header" id="home">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark pl-0 pr-0">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon fa fa-bars"></span>
                </button>

                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "",
                    Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "main",
                        "USE_EXT" => "N"
                    )
                );?><br>
<!--                <div class="collapse navbar-collapse" id="navbarNav">-->
<!--                    <ul class="navbar-nav ml-auto">-->
<!--                        <li class="nav-item active mr-lg-4">-->
<!--                            <a class="nav-link pl-0 pr-0" href="index.html">Home <span class="sr-only">(current)</span></a>-->
<!--                        </li>-->
<!--                        <li class="nav-item mr-lg-4">-->
<!--                            <a class="nav-link pl-0 pr-0" href="about.html">About me</a>-->
<!--                        </li>-->
<!--                        <li class="nav-item mr-lg-4">-->
<!--                            <a class="nav-link pl-0 pr-0" href="services.html">Services</a>-->
<!--                        </li>-->
<!--                        <li class="nav-item">-->
<!--                            <a class="nav-link pl-0 pr-0" href="contact.html">Contact</a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
            </nav>
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/logo.php"
                )
            ); ?>
        </div>
    </div>
</header>
<!-- //header -->
<!--  Main banner section -->
<div class="w3l-about py-5">
    <div class="container py-lg-3">