<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<div class="mfeedback">
    <? if (!empty($arResult["ERROR_MESSAGE"])) {
        foreach ($arResult["ERROR_MESSAGE"] as $v)
            ShowError($v);
    }
    if (strlen($arResult["OK_MESSAGE"]) > 0) {
        ?>
        <div class="mf-ok-text"><?= $arResult["OK_MESSAGE"] ?></div><?
    }
    ?>

    <form action="<?= POST_FORM_ACTION_URI ?>" method="POST">
        <div class="main-input">
            <?= bitrix_sessid_post() ?>
            <div class="mf-name">
                <div class="mf-text">
                    <?= GetMessage("MFT_NAME") ?><? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])): ?>
                        <span class="mf-req">*</span><? endif ?>
                </div>
                <input type="text" name="user_name" class="contact-input" value="<?= $arResult["AUTHOR_NAME"] ?>">
            </div>
            <div class="mf-email">
                <div class="mf-text">
                    <?= GetMessage("MFT_EMAIL") ?><? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])): ?>
                        <span class="mf-req">*</span><? endif ?>
                </div>
                <input type="text" name="user_email" class="contact-input" value="<?= $arResult["AUTHOR_EMAIL"] ?>">
            </div>

            <div class="mf-message">
                <div class="mf-text">
                    <?= GetMessage("MFT_MESSAGE") ?><? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])): ?>
                        <span class="mf-req">*</span><? endif ?>
                </div>
                <textarea name="MESSAGE" class="contact-textarea contact-input"><?= $arResult["MESSAGE"] ?></textarea>
            </div>
            <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
            <div class="text-right">
                <button class="btn-primary btn btn-style"><?= GetMessage("MFT_SUBMIT") ?></button>
            </div>

        </div>
    </form>
</div>