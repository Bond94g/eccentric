<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>

    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <? foreach ($arResult as $arItem): ?>
                <? if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                    continue; ?>
                <? if ($arItem["SELECTED"]): ?>
                    <li class="nav-item active mr-lg-4">
                        <a class="nav-link pl-0 pr-0" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?> <span
                                    class="sr-only">(current)</span></a>
                    </li>
                <? else: ?>
                    <li class="nav-item mr-lg-4">
                        <a class="nav-link pl-0 pr-0" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?> <span
                                    class="sr-only">(current)</span></a>
                    </li>
                <? endif; ?>

            <? endforeach; ?>
        </ul>
    </div>
<? endif ?>