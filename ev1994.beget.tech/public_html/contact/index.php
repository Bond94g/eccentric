<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Contact");
?><div class="contacts12-main">
	<div class="title-section">
		<h3 class="main-title-w3 mb-md-5 mb-4">Want to get in touch? <br>
		 Find me on <a href="tel:+12-331-456-789" class="editContent">phone</a>, <a href="mailto:mymail@mail.com" class="editContent">email</a>, <a href="#twitter" class="editContent">twitter</a>
		or <a href="#linkedin" class="editContent">linkedin</a>.</h3>
	</div>
		 <?$APPLICATION->IncludeComponent("bitrix:main.feedback", "contact_form", Array(
	"EMAIL_TO" => "evnin1960@gmail.com",	// E-mail, на который будет отправлено письмо
		"EVENT_MESSAGE_ID" => "",	// Почтовые шаблоны для отправки письма
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",	// Сообщение, выводимое пользователю после отправки
		"REQUIRED_FIELDS" => "",	// Обязательные поля для заполнения
		"USE_CAPTCHA" => "N",	// Использовать защиту от автоматических сообщений (CAPTCHA) для неавторизованных пользователей
	),
	false
);?>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>