<div class="row">
    <div class="col-lg-4 content-photo">
        <a href="#"><img src="<?= DEFAULT_TEMPLATE_PATH; ?>/images/myimage.jpg" class="img-fluid"
                         alt="content-photo"></a>
    </div>
    <div class="col-lg-8 content-left mt-lg-0 mt-5 ">
        <h3>I'm a Passionate designer & developer who loves simplicity in things and crafts beautiful
            user interfaces with love.</h3>
        <p class="mb-0">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Amet non porro
            laboriosam rerum fugiat quod ullam earum
            dignissimos corporis, nemo provident nostrum, nihil culpa. Et corrupti sit hic amet, animi
            unde cumque consequuntur omnis ad nihil optio
            id eum qui, impedit deleniti? Veniam eum aspernatur incidunt? Doloremque, cum? Repellendus
            consectetur, cupiditate tenetur provident
            neque, quas, totam eveniet nisi eius veritatis ea maiores ducimus a reprehenderit minima
            magnam dicta! Aliquam libero voluptatum facilis
            dolorum architecto? Doloribus fuga voluptate voluptatem corporis rem! Culpa nam et accusamus
            beatae!</p>
        <a href="about.html" class="btn-style btn-primary btn mt-lg-5 mt-4">About Me</a>
    </div>
</div>

