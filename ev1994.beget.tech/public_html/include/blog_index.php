<section class="w3l-blog py-5">
    <div class="container py-lg-5 py-md-3">
        <div class="row">
            <div class="col-lg-5">
                <h3>I'm Available For Freelance Projects.</h3>
                <h5 class="mt-3">Imagination is more important than knowledge </h5>
                <p class="mt-4"> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore minima
                    doloribus
                    provident, quia, ipsam totam
                    veniam iste ea temporibus non consequatur tenetur? Velit officiis laborum molestias ratione
                    impedit, totam, praesentium a nulla
                    fuga aliquam repellendus facilis consequuntur corrupti cum delectus, quia maiores accusamus
                    animi obcaecati vel ipsum.</p>
            </div>
            <div class="col-lg-7 mt-lg-0 mt-4">
                <div class="img-block">
                    <a href="single.html">
                        <img src="<?= DEFAULT_TEMPLATE_PATH; ?>/images/g3.jpg" class="img-fluid" alt="image"/>
                        <span>A collection of Creative Website Design</span>
                    </a>
                </div>
                <div class="img-block mt-3">
                    <a href="single.html"> <img src="<?= DEFAULT_TEMPLATE_PATH; ?>/images/g2.jpg" class="img-fluid"
                                                alt="image"/>
                        <span>Personalized Portfolio work</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
