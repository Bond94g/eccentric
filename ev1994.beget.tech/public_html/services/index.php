<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Services");
?><div class="title-section">
	<h3 class="main-title-w3">My Services</h3>
	<div class="title-line">
	</div>
</div>
<div class="row w3-services-grids mt-lg-5 mt-4">
	<div class="col-lg-5 w3-services-left-grid">
		<h4>What i do</h4>
		<p>
			 Lorem ipsum dolor sit amet, init sed adipisci ngelit. In a et euismod faucibus quam, a sodales er osplacerat vitae. Sed pretium fermentum luctus.Cras sodales nisl vitae dolor facilisis dapibus. Integer consectetur in velit eget viverra. Quisque vulputate a nisi blandit molestie. Aenean sit amet consequat risus, eget egestas est.Nullam eu turpis diam. Ut ac erat vestibulum, laoreet ex faucibus, iaculis ex. Donec at dolor volutpat, laoreet nisi.
		</p>
		<div class="more">
 <a href="#more" class="btn-primary btn btn-style mt-lg-5 mt-4">Know more</a>
		</div>
	</div>
	<div class="col-lg-7 w3-services-right-grid mt-lg-0 mt-5 pl-lg-5">
		<div class="row w3-icon-grid-gap1">
			<div class="col-md-6 col-sm-6 w3-icon-grid1">
 <a href="#link"> <span class="fa fa-codepen editContent" aria-hidden="true"></span>
				<h3>Web design</h3>
				<div class="clearfix">
				</div>
 </a>
				<p>
					 Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.
				</p>
			</div>
			<div class="col-md-6 col-sm-6 w3-icon-grid1">
 <a href="#link"> <span class="fa fa-mobile editContent" aria-hidden="true"></span>
				<h3>Mobile Apps</h3>
				<div class="clearfix">
				</div>
 </a>
				<p>
					 Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.
				</p>
			</div>
			<div class="col-md-6 col-sm-6 w3-icon-grid1">
 <a href="#link"> <span class="fa fa-hourglass editContent" aria-hidden="true"></span>
				<h3>Animation</h3>
				<div class="clearfix">
				</div>
 </a>
				<p>
					 Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.
				</p>
			</div>
			<div class="col-md-6 col-sm-6 w3-icon-grid1">
 <a href="#link"> <span class="fa fa-modx editContent" aria-hidden="true"></span>
				<h3>Photoshop</h3>
				<div class="clearfix">
				</div>
 </a>
				<p>
					 Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.
				</p>
			</div>
			<div class="col-md-6 col-sm-6 w3-icon-grid1">
 <a href="#link"> <span class="fa fa-bar-chart editContent" aria-hidden="true"></span>
				<h3>Marketing</h3>
				<div class="clearfix">
				</div>
 </a>
				<p>
					 Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.
				</p>
			</div>
			<div class="col-md-6 col-sm-6 w3-icon-grid1">
 <a href="#link"> <span class="fa fa-shopping-bag editContent" aria-hidden="true"></span>
				<h3>Development</h3>
				<div class="clearfix">
				</div>
 </a>
				<p>
					 Lorem ipsum dolor sit amet, init sed adipisci ngelit. In euismod faucibus.
				</p>
			</div>
		</div>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"template_news",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "template1",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_TEXT",2=>"",),
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "4",
        "IBLOCK_TYPE" => "content",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "20",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(0=>"ICON",1=>"",),
        "SET_BROWSER_TITLE" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
    )
);?>

 <!-- //services block3 --> <!-- services block4 -->
<div class="w3l-services py-5">
	<div class="container principles-grids principles-grids1 py-lg-3">
		<div class="scrollbar -services-scroll">
			<div class="row abt-btm pt-4">
				<div class="col-lg-3 col-sm-6 bottom-gds">
					<div class="elite-services1">
						<div class="bott-img">
							<div class="icon-holder editContent">
 <span class="fa fa-laptop service-icon" aria-hidden="true"></span>
							</div>
							<h4 class="mission">Webdesign</h4>
							<div class="description">
								<p>
									 Enean eget dolor dolor sit amet commodo ligula
								</p>
 <a href="#read" class="editContent">Read More</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6 bottom-gds mt-md-0 mt-5">
					<div class="elite-services2">
						<div class="bott-img">
							<div class="icon-holder editContent">
 <span class="fa fa-grav service-icon aria-hidden="></span>
							</div>
							<h4 class="mission">Graphic design</h4>
							<div class="description">
								<p>
									 Commodo ligula eget dolor dolor sit amet ligula
								</p>
 <a href="#read" class="editContent">Read More</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6 bottom-gds mt-lg-0 mt-5">
					<div class="elite-services3">
						<div class="bott-img">
							<div class="icon-holder editContent">
 <span class="fa fa-caret-square-o-up service-icon" aria-hidden="true"></span>
							</div>
							<h4 class="mission">UI Design</h4>
							<div class="description">
								<p>
									 Enean eget dolor dolor sit amet commodo ligula
								</p>
 <a href="#read" class="editContent">Read More</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6 bottom-gds mt-lg-0 mt-5">
					<div class="elite-services4">
						<div class="bott-img">
							<div class="icon-holder editContent">
 <span class="fa fa-adjust service-icon" aria-hidden="true"></span>
							</div>
							<h4 class="mission">Branding</h4>
							<div class="description">
								<p>
									 Commodo ligula eget dolor dolor sit amet ligula
								</p>
 <a href="#read" class="editContent">Read More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	 <!-- move top --> <button onclick="topFunction()" id="movetop" class="editContent" title="Go to top"> <span class="fa fa-angle-up"></span> </button>
	<script>
            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function () {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("movetop").style.display = "block";
                } else {
                    document.getElementById("movetop").style.display = "none";
                }
            }

            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }
        </script> <!-- /move top --> <!-- common jquery --> <script src="../local/templates/.default/js/jquery-3.3.1.min.js"></script> <!-- //common jquery --> <!-- disable body scroll which navbar is in active --> <script>
            $(function () {
                $('.navbar-toggler').click(function () {
                    $('body').toggleClass('noscroll');
                })
            });
        </script> <!-- disable body scroll which navbar is in active --> <!-- for blog carousel slider --> <script src="../local/templates/.default/js/owl.carousel.js"></script> <script>
            $(document).ready(function () {
                var owl = $('.owl-carousel');
                owl.owlCarousel({
                    stagePadding: 20,
                    margin: 15,
                    nav: false,
                    loop: false,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        1000: {
                            items: 3
                        }
                    }
                })
            })
        </script> <!-- //for blog carousel slider --> <!-- stats number counter--> <script src="../local/templates/.default/js/jquery.waypoints.min.js"></script> <script src="../local/templates/.default/js/jquery.countup.js"></script> <script>
            $('.counter').countUp();
        </script> <!-- //stats number counter --> <!-- jQuery-Photo-filter-lightbox-portfolio-plugin --> <script src="../local/templates/.default/js/jquery-1.7.2.js"></script> <script src="../local/templates/.default/js/jquery.quicksand.js"></script> <script src="../local/templates/.default/js/script.js"></script> <script src="../local/templates/.default/js/jquery.prettyPhoto.js"></script> <!-- //jQuery-Photo-filter-lightbox-portfolio-plugin --> <!--  bootstrap js --> <script src="../local/templates/.default/js/bootstrap.min.js"></script> <!--  //bootstrap js -->
</div>
 <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>